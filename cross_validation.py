import pandas as pd
import matplotlib.pyplot as plt

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)

df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
#df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

"""
"" Transformacion de distintas clases o cetegorias a valores Ordinales
"" Ventajas?
"""
from sklearn import preprocessing
labelEncoder = preprocessing.LabelEncoder()
df = df.apply(labelEncoder.fit_transform)
#print("clases de label encoder {lec}".format(lec=labelEncoder.classes_))

"""
"" Eliminamos los datos faltantes reemplazamos por la media de la columna
"""
from sklearn.preprocessing import Imputer
imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)
df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

"""
"" Separamos features del labels
"""
X = df.drop('Calificacion', 1)
y = df.Calificacion
#print(X.shape)


"""
"" Testing using dummies
"""
todo_list = ['CiudadNacimiento']#,'FranjaEdad','NivelMaximoEducacion', 'Idiomas', 'ExperienciaPuesto', 'PromedioOtrosEmpleos', 'HabilidadLiderazgo', 'HabilidadResponsabilidad', 'HabilidadCreatividad', 'HabilidadSocial']

#print(X.head(5))
def dummy_df(data, f_list):
	for x in f_list:
		dummies = pd.get_dummies(data[x], prefix=x, dummy_na=False)
		data = data.drop(x, 1)
		data = pd.concat([data, dummies], axis=1)
	return data

X_sparsed = dummy_df(X, todo_list)


print("Forma X {x_shape}".format(x_shape=X_sparsed.shape))
print("Forma y {y_shape}".format(y_shape=y.shape))

"""
"" One Hot Encoding
"" OHE is a representation method that takes each category value and turns it into a binary vector of size |i|(number of values in category i)
"" A one hot encoding allows the representation of categorical data to be more expressive.
"" As this is ordinal characteristic is usually not desired, one hot encoding is necessary for the proper representation of the distinct elements of the variable
"" In numerical analysis and computer science, a sparse matrix or sparse array is a matrix in which most of the elements are zero. By contrast, if most of the elements are nonzero, then the matrix is considered dense
"" https://en.wikipedia.org/wiki/Sparse_matrix
"""
"""
from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(handle_unknown='ignore')
X_OHE = enc.fit(X_sparsed)

print(X_OHE.feature_indices_)
print(X_OHE.n_values_)

X_transformed = enc.fit_transform(X_sparsed).toarray()

print(X_transformed.shape)
print(y.shape)
"""


#from sklearn.cross_validation import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X_sparsed, y, test_size=0.20, random_state=33)
from sklearn.model_selection import KFold # import KFold
kf = KFold(n_splits=2) # Define the split - into 2 folds 
kf.get_n_splits(X_sparsed) # returns the number of splitting iterations in the cross-validator
print(kf) 

for train_index, test_index in kf.split(X_sparsed):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X_sparsed[train_index], X_sparsed[test_index]
    y_train, y_test = y[train_index], y[test_index]