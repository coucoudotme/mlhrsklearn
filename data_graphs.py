import pandas as pd
import matplotlib.pyplot as plt

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]

df = df.drop('Nacionalidad',1)

print(df['Calificacion'].value_counts())
# import preprocessing from sklearn
from sklearn import preprocessing

labelEncoder = preprocessing.LabelEncoder()

df = df.apply(labelEncoder.fit_transform)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)

df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

"""
"" 	mezcla la data ya que esta ordenada
"""
df = df.sample(frac=1).reset_index(drop=True)

#plt.style.use('ggplot')
#pd.DataFrame.hist(df, figsize = [15,15])
#plt.show()

df.to_csv('../datasets/data_exported.csv')
