import csv
import json

"""
"" Script that inports data from the raw csv and adapt it to sklearn
"""
csvpath = '../datasets/ml_1.csv'
jsonfile = '../datasets/ml_1.json'


#Load CSV and build the object
csvrows = []

"""
"" Raw Format
{
	"FranjaEdad": "35_50",
	"Sexo": "M",
	"Nacionalidad": "ARG",
	"CiudadNacimiento": "CAP FED",
	"NivelMaximoEducacion": "UNIVPRIV_8_10",
	"Idiomas": "DOS",
	"ExperienciaPuesto": "SI",
	"PromedioOtrosEmpleos": "6_10",
	"HabilidadLiderazgo": "SI",
	"HabilidadSocial": "SI",
	"HabilidadCreatividad": "NO",
	"HabilidadResponsabilidad": "SI",
	"Calificacion": "SUPERA EXPECTATIVAS"
}

"" Pocessed Format
{
	"FranjaEdad": 1,
	"NivelMaximoEducacion": 2,
	"Idiomas": 1,
	"ExperienciaPuesto": 1,
	"PromedioOtrosEmpleos": 2,
	"HabilidadLiderazgo": 1,
	"HabilidadSocial": 1,
	"HabilidadCreatividad": 0,
	"HabilidadResponsabilidad": 1,
	"Calificacion": 1
}
"""
with open(csvpath) as csvfile:
	reader = csv.DictReader(csvfile)
	title = reader.fieldnames
	
	for row in reader:
		raw = {title[i]:row[title[i]] for i in range(len(title))}

		"""
		"" Mapping and num transformation
		"""
		del raw['Sexo']
		del raw['Nacionalidad']
		del raw['CiudadNacimiento']

		if raw['FranjaEdad'] == '<35':
			raw['FranjaEdad'] = 0
		if raw['FranjaEdad'] == '35_50':
			raw['FranjaEdad'] = 1
		if raw['FranjaEdad'] == '>50':
			raw['FranjaEdad'] = 2

		if raw['NivelMaximoEducacion'] == 'SECUNDARIA':
			raw['NivelMaximoEducacion'] = 0
		if raw['NivelMaximoEducacion'] == 'UNIVPUB_6_4':
			raw['NivelMaximoEducacion'] = 1
		if raw['NivelMaximoEducacion'] == 'UNIVPRIV_8_10':
			raw['NivelMaximoEducacion'] = 2
		if raw['NivelMaximoEducacion'] == 'UNIVPUB_7_10':
			raw['NivelMaximoEducacion'] = 3
		if raw['NivelMaximoEducacion'] == 'UNIVPRIV_4_7':
			raw['NivelMaximoEducacion'] = 4

		if raw['Idiomas'] == 'UNO':
			raw['Idiomas'] = 0
		if raw['Idiomas'] == 'DOS':
			raw['Idiomas'] = 1
		if raw['Idiomas'] == 'TRES':
			raw['Idiomas'] = 2
		if raw['Idiomas'] == 'CUATRO':
			raw['Idiomas'] = 3

		if raw['PromedioOtrosEmpleos'] == '1_2':
			raw['PromedioOtrosEmpleos'] = 0
		if raw['PromedioOtrosEmpleos'] == '3_6':
			raw['PromedioOtrosEmpleos'] = 1			
		if raw['PromedioOtrosEmpleos'] == '6_10':
			raw['PromedioOtrosEmpleos'] = 2
		if raw['PromedioOtrosEmpleos'] == 'Mayor_10':
			raw['PromedioOtrosEmpleos'] = 3

		raw['ExperienciaPuesto'] = 0 if raw['ExperienciaPuesto'] == 'NO' else 1

		raw['HabilidadLiderazgo'] = 0 if raw['HabilidadLiderazgo'] == 'NO' else 1

		raw['HabilidadSocial'] = 0 if raw['HabilidadSocial'] == 'NO' else 1		

		raw['HabilidadCreatividad'] = 0 if raw['HabilidadCreatividad'] == 'NO' else 1

		raw['HabilidadResponsabilidad'] = 0 if raw['HabilidadResponsabilidad'] == 'NO' else 1	

		raw['Calificacion'] = 0 if raw['Calificacion'] == 'NO CUMPLE EXPECTATIVAS' else 1	

		csvrows.extend([raw])

	#print(csvrows)

# Write Json
with open(jsonfile, "w") as f:
	f.write(json.dumps(csvrows))

		