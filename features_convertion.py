import pandas as pd
import matplotlib.pyplot as plt

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)

df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
#df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

labels_names=df.Calificacion

"""
"" Transformacion de distintas clases o cetegorias a valores Ordinales
"" Ventajas?
"""
from sklearn import preprocessing
labelEncoder = preprocessing.LabelEncoder()
df = df.apply(labelEncoder.fit_transform)
#print("clases de label encoder {lec}".format(lec=labelEncoder.classes_))

"""
"" Eliminamos los datos faltantes reemplazamos por la media de la columna
"""
from sklearn.preprocessing import Imputer
imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)
df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

"""
"" Separamos features del labels
"""
X = df.drop('Calificacion', 1)
y = df.Calificacion
#print(X.shape)


"""
"" Testing using dummies
"""
todo_list = ['CiudadNacimiento']#,'FranjaEdad','NivelMaximoEducacion', 'Idiomas', 'ExperienciaPuesto', 'PromedioOtrosEmpleos', 'HabilidadLiderazgo', 'HabilidadResponsabilidad', 'HabilidadCreatividad', 'HabilidadSocial']

#print(X.head(5))
def dummy_df(data, f_list):
	for x in f_list:
		dummies = pd.get_dummies(data[x], prefix=x, dummy_na=False)
		data = data.drop(x, 1)
		data = pd.concat([data, dummies], axis=1)
	return data

X_sparsed = dummy_df(X, todo_list)


#print("Forma X {x_shape}".format(x_shape=X_sparsed.shape))
#print("Forma y {y_shape}".format(y_shape=y.shape))

"""
"" One Hot Encoding
"" OHE is a representation method that takes each category value and turns it into a binary vector of size |i|(number of values in category i)
"" A one hot encoding allows the representation of categorical data to be more expressive.
"" As this is ordinal characteristic is usually not desired, one hot encoding is necessary for the proper representation of the distinct elements of the variable
"" In numerical analysis and computer science, a sparse matrix or sparse array is a matrix in which most of the elements are zero. By contrast, if most of the elements are nonzero, then the matrix is considered dense
"" https://en.wikipedia.org/wiki/Sparse_matrix
"""
"""
from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(handle_unknown='ignore')
X_OHE = enc.fit(X_sparsed)

print(X_OHE.feature_indices_)
print(X_OHE.n_values_)

X_transformed = enc.fit_transform(X_sparsed).toarray()

print(X_transformed.shape)
print(y.shape)
"""


from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X_sparsed, y, test_size=0.20, random_state=33)

#print("X_train:{X_train}".format(X_train=X_train.shape))
#print("X_test:{X_test}".format(X_test=X_test.shape))
#print("y_train:{y_train}".format(y_train=y_train.shape))
#print("y_test:{y_test}".format(y_test=y_test.shape))


"""
"" http://adataanalyst.com/scikit-learn/decision-trees-scikit-learn/
"" Entropy is a measure of disorder in a set
"""
from sklearn import tree
t = tree.DecisionTreeClassifier(criterion = "entropy", random_state = 100, max_depth=3, min_samples_leaf=5)
t = t.fit(X_train,y_train)

from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics



knn = KNeighborsClassifier(n_neighbors=24,  algorithm='auto')
knn.fit(X_train, y_train)

#print('k-NN score for test set: %f' % knn.score(X_test, y_test))
#print('k-NN score for training set: %f' % knn.score(X_train, y_train))

#from sklearn.linear_model import LogisticRegression
#lr = LogisticRegression(random_state=0)
#lr.fit(X_train, y_train)

"""
"" https://datascienceplus.com/building-a-logistic-regression-in-python-step-by-step/
"""


def measure_performance(X,y,clf, show_accuracy=True, show_classification_report=True, show_confusion_matrix=True):
    y_pred=clf.predict(X)   
    if show_accuracy:
        print ("Accuracy:{0:.3f}".format(metrics.accuracy_score(y,y_pred)),"\n")

    if show_classification_report:
        print ("Classification report")
        print (metrics.classification_report(y,y_pred),"\n")
        
    if show_confusion_matrix:
        print ("Confusion matrix")
        print (metrics.confusion_matrix(y,y_pred),"\n")
        ppp = pd.crosstab(y, y_pred, rownames=['True'], colnames=['Predicted'], margins=True)
        print(ppp,"\n")
        #conf = metrics.confusion_matrix(y, y_pred)
        #plt.imshow(conf, cmap='binary', interpolation='None')
        #plt.show()
print(X_test.shape)
print("KNN ---------------------------------","\n")
measure_performance(X_test,y_test, knn, show_classification_report=True, show_confusion_matrix=True)
print("Arboles --------------------------------------","\n")
#measure_performance(X_test,y_test, lr, show_classification_report=True, show_confusion_matrix=True)
#print("--------------------------------------","\n")
measure_performance(X_test,y_test, t, show_classification_report=True, show_confusion_matrix=True)

#from sklearn.metrics import classification_report
#y_true, y_pred = y_test, knn.predict(X_test)
#print(classification_report(y_true, y_pred))

import seaborn as sns
sns.distplot(y_test);

#sns.heatmap(X_train.corr(), annot=True, fmt="f")
#sns.countplot(y="PromedioOtrosEmpleos", data=df)
#plt.show()
#sns.heatmap(X_sparsed.corr(), cmap="YlGnBu")
plt.show()

"""
"" Conversion de los valores de preocupacional ponderados por importancia para el puesto
"" Suponemos un valor entre 0-1 para cada uno:
"" HabilidadLiderazgo : 0.7
"" HabilidadSocial : 0.4
"" HabilidadCreatividad: 0.6
"" HabilidadResponsabilidad : 0.8
"" Modelo de Rash http://www.psicothema.com/pdf/1029.pdf
"" Ejemplo: 0.7*1 + 0.4*0 + 0.6*1 + 0.8*1 = 0.7 + 0.6 + 0.8 = 2.1
"" Max: 0.7*1 + 0.4*1 + 0.6*1 + 0.8*1 = 0.7 + 0.4 + 0.6 + 0.8 = 2.5
"""



"""
"" Con solo transformar en ordinal no alcanza, suponemos que si tenemos Ciudades New York 1 Buenos Aires 2, el algoritmo podría
"" asumir que los mismos valore se parecen (hacer analogia con RR.HH)
"""

"""
"" Dibujando el arbol
"""
print(X_sparsed.columns.values)
print(labels_names.unique())
with open("hrr_tree_classifier.dot", "w") as f:
    f = tree.export_graphviz(t, out_file=f, feature_names=X_sparsed.columns.values,class_names=labels_names.unique(), filled=True, rounded=True)

from subprocess import check_call
check_call(['dot','-Tpng','hrr_tree_classifier.dot','-o','hrr_tree_classifier.png'])