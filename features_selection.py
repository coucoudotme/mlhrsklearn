import pandas as pd
import matplotlib.pyplot as plt

"""
"" Fetaures selection
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)

"""
""	Cantidad de categoria por variables, esto permite revisar aquellas variables con muchas categorias distintas y reagruparlas
""	De esta manera se pueden agrupar aquellas que no tienen mucha frecuencia en otra llamada "Otros"
"""
for col_name in df.columns:
	if df[col_name].dtypes =='object':
		unique_cat = len(df[col_name].unique())
		print("Feature '{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))

#print(df['Nacionalidad'].value_counts().sort_values(ascending=False).head(10))

#print(df['Nacionalidad'].describe())
#df['Nacionalidad'].value_counts().plot(kind='bar')
#plt.show()
# Muestra el nombre de las columnas
#print(df['Nacionalidad'].unique())
#	Cuenta la distribucion o frecuencia
#print(df['Nacionalidad'].value_counts())

print(df['CiudadNacimiento'].describe())

print("Analizando CiudadNacimiento ya que tiene multiples clases:")
df['CiudadNacimiento'].value_counts().plot(kind='bar')
plt.show()
print(df['CiudadNacimiento'].value_counts().sort_values(ascending=False).head(10))

percetage = df['CiudadNacimiento'].value_counts()/len(df['CiudadNacimiento'])
print(percetage)