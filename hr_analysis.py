"""
"" HR Data
"" Load datasets http://scikit-learn.org/stable/datasets/index.html
""
"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo'
""	Feature 'Nacionalidad'
""	Feature 'CiudadNacimiento'
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/ml_1_a.csv",  warn_bad_lines=True, encoding="cp1252")

"""
""	0. Observacion de la data
""	#########################
"""

"""
""	Descripcion grafica de la frecuencia de cada variable 
""
""	#	Describe la frecuencia de la variable
""	print(df['Calificacion'].describe())
""
""	df['Calificacion'].value_counts().plot(kind='bar')
""	plt.show()
""
""	# Muestra el nombre de las columnas
""	print(df['Calificacion'].unique())
"" 
""	#	Cuenta la distribucion o frecuencia
""	print(df['Calificacion'].value_counts())
"""

"""
""	1- Preprocesamiento de la información
""	#####################################
"""

"""
"" Separamos los features de los labels
"""

X = df.drop('Calificacion', 1)
y = df.Calificacion

"""
""	Convertimos los valores de Calificacion a classes que se puedan manejar
""	Dada la muestra se encuentran las clases SUPERA ESPECTATIVAS y NO SUPERA ESPECTATIVAS
"""
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)

"""
""	Cantidad de categoria por variables, esto permite revisar aquellas variables con muchas categorias distintas y reagruparlas
""	De esta manera se pueden agrupar aquellas que no tienen mucha frecuencia en otra llamada "Otros"
"""
for col_name in df.columns:
	if df[col_name].dtypes =='object':
		unique_cat = len(df[col_name].unique())
		print("Feature '{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))

"""
"" Por ejemplo Ciudad de Nacimiento tiene 101 categorias distintas
"" Vemos entonces la distribucion de cada una de las categorias
""	Feature 'FranjaEdad' tiene '3' categorias unicas
""	Feature 'Sexo' tiene '1' categorias unicas
""	Feature 'Nacionalidad' tiene '10' categorias unicas
""	Feature 'CiudadNacimiento' tiene '101' categorias unicas
""	Feature 'NivelMaximoEducacion' tiene '5' categorias unicas
""	Feature 'Idiomas' tiene '4' categorias unicas
""	Feature 'ExperienciaPuesto' tiene '2' categorias unicas
""	Feature 'PromedioOtrosEmpleos' tiene '4' categorias unicas
""	Feature 'HabilidadLiderazgo' tiene '2' categorias unicas
""	Feature 'HabilidadSocial' tiene '2' categorias unicas
""	Feature 'HabilidadCreatividad' tiene '2' categorias unicas
""	Feature 'HabilidadResponsabilidad' tiene '2' categorias unicas
""	Feature 'Calificacion' tiene '2' categorias unicas
"""
#print("Analizando Nacionalidad ya que tiene multiples clases:")
#print(df['Nacionalidad'].value_counts().sort_values(ascending=False).head(10))
"""
""	ARG    665
""	ESP      2
""	URU      2
""	ITA      2
""	PER      1
""	CHI      1
""	CDA      1
""	FRA      1
""	EUA      1
""	POR      1
"" Claramente la frecuencia se concentra en Argentina, con lo cual se pueden agrupar en ARG y Otros
"""

"""
""	En este ejemplo dejamos de lado los paises, ya que pretendemos euna demo para ARG
"" 	Elimino el feature Nacionalidad
"""
#X = X.drop('Nacionalidad', 1)
#print(X.head(5))

print("Analizando CiudadNacimiento ya que tiene multiples clases:")
print(df['CiudadNacimiento'].value_counts().sort_values(ascending=False).head(10))
"""
""	CAP FED            423
""	QUILMES             36
""	LOMAS DE ZAMORA     20
""	AVELLANEDA          12
""	LANUS                9
""	SALTA                8
""	SAN ISIDRO           8
""	SAN JUAN             7
""	SANTA FE             6
""	LA PLATA             6
""	Existen tantas otras con frecuencia 1, con lo cual se pueden agrupar en CAP FED y quizas otras mas pero si se puede reducir
"""

"""
"" Tomamo la ciudad y agrupamos aquellas con baja frecuencia, solo dejamos Capital Federal y sumamos Others
"" Este feature no tiene mucha relevancia en el tratamiento, revisar si esto aplica.
"""
X['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in X['CiudadNacimiento']]
#print(X['CiudadNacimiento'].value_counts().sort_values(ascending=False))

"""
""	Realizamos la transformacion de las variables o features a sus descripciones numericas de clase como se hizo con los labels
""	Eliminamos los features para representarlos en 1 y 0 para cada categoria
"""
todo_list = ['FranjaEdad','Sexo', 'CiudadNacimiento', 'NivelMaximoEducacion', 'Idiomas', 'ExperienciaPuesto', 'PromedioOtrosEmpleos', 'HabilidadLiderazgo', 'HabilidadResponsabilidad', 'HabilidadCreatividad', 'HabilidadSocial']

#print(X.head(5))
def dummy_df(data, f_list):
	for x in f_list:
		dummies = pd.get_dummies(data[x], prefix=x, dummy_na=False)
		data = data.drop(x, 1)
		data = pd.concat([data, dummies], axis=1)
	return data

X = dummy_df(X, todo_list)
#print(X.head(5))

"""
"" Imputer para remover NaN values
"" Se verifca si hay data perdida N/A
"""
from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
imp.fit(X)

X = pd.DataFrame(data=imp.transform(X), columns = X.columns)

print(X.isnull().sum().sort_values(ascending=False).head())


"""
""	Outlier detection o deteccion de patrones raros
""	Observacion que se desvia drasticamente de otro observacion del dataset
"""
#TODO

"""
""	# Grafica de histograma para una variable
""	plt.hist(df['HL'], color='gray', alpha=0.5)
""	plt.title("Histograma de '{var_name}'".format(var_name=df['HL'].name))
""	plt.xlabel('Valor')
""	plt.ylabel('Frecuancia')
""	plt.show()
"""

"""
"" Histograma, relacion entre dos variables
"""
def plot_histogram_DV(x, y):
	plt.hist(list(x[y==0]), alpha=0.5, label='DV=0')
	plt.hist(list(x[y==1]), alpha=0.5, label='DV=1')
	plt.title("Histograma de '{var_name}' por Categoria DV".format(var_name=x.name))
	plt.xlabel('Valor')
	plt.ylabel('Frecuencia')
	plt.show()

# EJEMPLO CON Sexo_M
#plot_histogram_DV(X['Sexo_M'], y)

"""
""	Interaciones entre los features
""	Es muy costoso computacionalmente ya que se agrega informacion nueva ... no lo incluimos, nos apoyamos en el conocimiento de los expertos
""	para seleccionar la informacion extraible de la data
"""

"""
""	Uso de PolinomianFeatures en sklear.preprocessing para crear two-way interacciones para todos los features 
"""
from itertools import combinations
from sklearn.preprocessing import PolynomialFeatures

def add_interactions(df):
	# Nombre de los features
	combos = list(combinations(list(df.columns), 2))
	colnames = list(df.columns) + ['_'.join(x) for x in combos]

	# Busqueda de las interacciones
	poly = PolynomialFeatures(interaction_only=True, include_bias=False)
	df = poly.fit_transform(df)
	df = pd.DataFrame(df)
	df.columns = colnames

	# Se remueven interacciones con todos sus componenetes 0
	noint_indicies = [i for i, x in enumerate(list((df == 0).all())) if x]
	df = df.drop(df.columns[noint_indicies], axis=1)

	return df

X = add_interactions(X)

print(X)


"""
"" Como la cantidad de features luego de la composicion crece mucho, existen metodos para reducirlos
"" PCA Principal component anaysis
"" La deventaja es que es muy dicil de leer luego
"" TODO: Revisar ventajas y desvenjas, como por ejemplo luego saber cual es cual
"" sklearn.decomposition import PCA
"" un parametro magico es 10
"""

"""
"" Creacion del modelo usando la data ya procesada
"""
#from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.40, random_state=1)

#print(df.shape)
# ver la cantidad de nuevos features creados
#print(X.shape)

"""
"" Dado la cantidad de features seleccionamos solos algunos
"""
import sklearn.feature_selection

"""
select = sklearn.feature_selection.SelectKBest(k=20)
selected_features = select.fit(X_train, y_train)
indices_selected = selected_features.get_support(indices = True)
colnames_selected = [X.columns[i] for i in indices_selected]

X_train_selected = X_train[colnames_selected]
X_test_selected = X_test[colnames_selected]

print(X_train_selected.shape)
print(X_test_selected.shape)
"""
#print(colnames_selected)

print(X_train.shape)
print(X_test.shape)

"""
"" Prueba de Logistic Regression
"""
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_auc_score
from sklearn import metrics

def find_model_perf_log_reg(X_train, y_train, X_test, y_test):
	model = LogisticRegression()
	model.fit(X_train, y_train)
	y_hat = [x[1] for x in model.predict_proba(X_test)]
	auc = roc_auc_score(y_test, y_hat)

	return auc

def find_model_perf_knn(X_train, y_train, X_test, y_test):
	model = KNeighborsClassifier(n_neighbors=30)
	model.fit(X_train, y_train)
	y_hat = [x[1] for x in model.predict_proba(X_test)]
	auc = roc_auc_score(y_test, y_hat)

	return auc

"""
"" El objetivo es buscar un modelo que generalice nuestra data
"" Identificar cuales son los parametros que mejor se ajustan para el modelo seleccionado
"" 
"""
auc_processed_log_reg = find_model_perf_log_reg(X_train, y_train, X_test, y_test)
#auc_processed_log_reg = find_model_perf_log_reg(X_train, y_train, X_test, y_test)
print('LogisticRegression: {response}'.format(response=auc_processed_log_reg))


auc_processed_knn = find_model_perf_knn(X_train, y_train, X_test, y_test)
#auc_processed_knn = find_model_perf_knn(X_train, y_train, X_test, y_test)
print('KNN: {response}'.format(response=auc_processed_knn))

