"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Imputer
from sklearn.utils import resample
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report


df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


#df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)
df = df.drop('HabilidadResponsabilidad',1)
df = df.drop('HabilidadLiderazgo',1)
df = df.drop('HabilidadCreatividad',1)
df = df.drop('HabilidadSocial',1)

print("Características:")
print("----------------")

for col_name in df.columns:
	if df[col_name].dtypes =='object':
		unique_cat = len(df[col_name].unique())
		print("'{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))

print(df['Calificacion'].value_counts())
print("\n")

print("Up sampling")
print("\n")

#UP SAMPLING
df_majority = df[df.Calificacion=='CUMPLE EXPECTATIVAS']
df_medium = df[df.Calificacion=='SUPERA EXPECTATIVAS']
df_minority = df[df.Calificacion=='NO CUMPLE EXPECTATIVAS']

# Upsample minority class
df_minority_upsampled = resample(df_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_medium_upsampled = resample(df_medium, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_upsampled = pd.concat([df_majority, df_minority_upsampled])
df_upsampled = pd.concat([df_upsampled, df_medium_upsampled])


# Display new class counts
print(df_upsampled.Calificacion.value_counts())
print("FranjaEdad")
print(df_upsampled.FranjaEdad.value_counts())
print("NivelMaximoEducacion")
print(df_upsampled.NivelMaximoEducacion.value_counts())
print("Idiomas")
print(df_upsampled.Idiomas.value_counts())
print("ExperienciaPuesto")
print(df_upsampled.ExperienciaPuesto.value_counts())
print("PromedioOtrosEmpleos")
print(df_upsampled.PromedioOtrosEmpleos.value_counts())


df_upsampled = df_upsampled.sample(frac=1).reset_index(drop=True)

labelEncoder = LabelEncoder()

X = df_upsampled.apply(labelEncoder.fit_transform)

y = df_upsampled.Calificacion
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)


X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.20, random_state=123)

# Train model
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

"""k_range = range(1, 26)
scores = []
for k in k_range:
	knn = knn = KNeighborsClassifier(n_neighbors=k)
	knn.fit(X_train, y_train)
	y_pred_knn = knn.predict(X_test)
	met = metrics.accuracy_score(y_test, y_pred_knn)
	print("met {met} k {k}".format(met=met, k=k))
	scores.append(met)
"""
# Train model
clf= KNeighborsClassifier(n_neighbors=2)

clf.fit(X_train, y_train)
y_true, pred = y_test, clf.predict(X_test)

# Is our model still predicting just one class?
print( np.unique( pred ) )
 
# How's our accuracy?
print( accuracy_score(y_true, pred) )

print(classification_report(y_true, pred))
#
print(confusion_matrix(y_true, pred))


