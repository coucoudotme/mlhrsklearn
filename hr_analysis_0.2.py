"""
"" HR Data
"" Load datasets http://scikit-learn.org/stable/datasets/index.html
""
"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo'
""	Feature 'Nacionalidad'
""	Feature 'CiudadNacimiento'
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/ml_1_a.csv",  warn_bad_lines=True, encoding="cp1252")

"""
"" Separamos los features de los labels
"""

X = df.drop('Calificacion', 1)
y = df.Calificacion

"""
""	Convertimos los valores de Calificacion a classes que se puedan manejar
""	Dada la muestra se encuentran las clases SUPERA ESPECTATIVAS y NO SUPERA ESPECTATIVAS
"""
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)

"""
""	Cantidad de categoria por variables, esto permite revisar aquellas variables con muchas categorias distintas y reagruparlas
""	De esta manera se pueden agrupar aquellas que no tienen mucha frecuencia en otra llamada "Otros"
"""
for col_name in df.columns:
	if df[col_name].dtypes =='object':
		unique_cat = len(df[col_name].unique())
		print("Feature '{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))

"""
"" Por ejemplo Ciudad de Nacimiento tiene 101 categorias distintas
"" Vemos entonces la distribucion de cada una de las categorias
""	Feature 'FranjaEdad' tiene '3' categorias unicas
""	Feature 'Sexo' tiene '1' categorias unicas
""	Feature 'Nacionalidad' tiene '10' categorias unicas
""	Feature 'CiudadNacimiento' tiene '101' categorias unicas
""	Feature 'NivelMaximoEducacion' tiene '5' categorias unicas
""	Feature 'Idiomas' tiene '4' categorias unicas
""	Feature 'ExperienciaPuesto' tiene '2' categorias unicas
""	Feature 'PromedioOtrosEmpleos' tiene '4' categorias unicas
""	Feature 'HabilidadLiderazgo' tiene '2' categorias unicas
""	Feature 'HabilidadSocial' tiene '2' categorias unicas
""	Feature 'HabilidadCreatividad' tiene '2' categorias unicas
""	Feature 'HabilidadResponsabilidad' tiene '2' categorias unicas
""	Feature 'Calificacion' tiene '2' categorias unicas
"""
#print("Analizando Nacionalidad ya que tiene multiples clases:")
#print(df['Nacionalidad'].value_counts().sort_values(ascending=False).head(10))
"""
""	ARG    665
""	ESP      2
""	URU      2
""	ITA      2
""	PER      1
""	CHI      1
""	CDA      1
""	FRA      1
""	EUA      1
""	POR      1
"" Claramente la frecuencia se concentra en Argentina, con lo cual se pueden agrupar en ARG y Otros
"""

"""
""	En este ejemplo dejamos de lado los paises, ya que pretendemos euna demo para ARG
"" 	Elimino el feature Nacionalidad
"""
X = X.drop('Nacionalidad', 1)
#print(X.head(5))

#print("Analizando CiudadNacimiento ya que tiene multiples clases:")
#print(df['CiudadNacimiento'].value_counts().sort_values(ascending=False).head(10))
"""
""	CAP FED            423
""	QUILMES             36
""	LOMAS DE ZAMORA     20
""	AVELLANEDA          12
""	LANUS                9
""	SALTA                8
""	SAN ISIDRO           8
""	SAN JUAN             7
""	SANTA FE             6
""	LA PLATA             6
""	Existen tantas otras con frecuencia 1, con lo cual se pueden agrupar en CAP FED y quizas otras mas pero si se puede reducir
"""

"""
"" Tomamo la ciudad y agrupamos aquellas con baja frecuencia, solo dejamos Capital Federal y sumamos Others
"" Este feature no tiene mucha relevancia en el tratamiento, revisar si esto aplica.
"""
X['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in X['CiudadNacimiento']]
#print(X['CiudadNacimiento'].value_counts().sort_values(ascending=False))

"""
""	Realizamos la transformacion de las variables o features a sus descripciones numericas de clase como se hizo con los labels
""	Eliminamos los features para representarlos en 1 y 0 para cada categoria
"""
todo_list = ['FranjaEdad','Sexo', 'CiudadNacimiento', 'NivelMaximoEducacion', 'Idiomas', 'ExperienciaPuesto', 'PromedioOtrosEmpleos', 'HabilidadLiderazgo', 'HabilidadResponsabilidad', 'HabilidadCreatividad', 'HabilidadSocial']

#print(X.head(5))
def dummy_df(data, f_list):
	for x in f_list:
		dummies = pd.get_dummies(data[x], prefix=x, dummy_na=False)
		data = data.drop(x, 1)
		data = pd.concat([data, dummies], axis=1)
	return data

X = dummy_df(X, todo_list)
#print(X.head(5))

"""
"" Imputer para remover NaN values
"" Se verifca si hay data perdida N/A
"""
from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(X)

X = pd.DataFrame(data=imp.transform(X), columns = X.columns)

print(X.isnull().sum().sort_values(ascending=False).head())


"""
""	Outlier detection o deteccion de patrones raros
""	Observacion que se desvia drasticamente de otro observacion del dataset
"""
#TODO

"""
""	# Grafica de histograma para una variable
""	plt.hist(df['HL'], color='gray', alpha=0.5)
""	plt.title("Histograma de '{var_name}'".format(var_name=df['HL'].name))
""	plt.xlabel('Valor')
""	plt.ylabel('Frecuancia')
""	plt.show()
"""

"""
"" Creacion del modelo usando la data ya procesada
"""
#from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.40, random_state=4)

print(X.shape)
print(X_train.shape)
print(X_test.shape)

print(y.shape)
print(y_train.shape)
print(y_test.shape)

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

lg = LogisticRegression()
lg.fit(X_train, y_train)
print(lg)

y_pred_lg = lg.predict(X_test)
print(metrics.accuracy_score(y_test, y_pred_lg))

knn = KNeighborsClassifier(n_neighbors=19)
knn.fit(X_train, y_train)
print(knn)

y_pred_knn = knn.predict(X_test)
print(metrics.accuracy_score(y_test, y_pred_knn))

k_range = range(1, 26)
scores = []
for k in k_range:
	knn = knn = KNeighborsClassifier(n_neighbors=k)
	knn.fit(X_train, y_train)
	y_pred_knn = knn.predict(X_test)
	met = metrics.accuracy_score(y_test, y_pred_knn)
	#print("met {met} k {k}".format(met=met, k=k))
	scores.append(met)

plt.plot(k_range, scores)
plt.xlabel("Valor de K para knn")
plt.ylabel("Prueba de certeza")
plt.show()