import pandas as pd
import matplotlib.pyplot as plt

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo'
""	Feature 'Nacionalidad'
""	Feature 'CiudadNacimiento'
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)

df = df.drop('Nacionalidad', 1)
df = df.drop('CiudadNacimiento',1)
df = df.drop('Sexo', 1)
df = df.drop('FranjaEdad', 1)
df = df.drop('NivelMaximoEducacion', 1)
df = df.drop('Idiomas', 1)
df = df.drop('ExperienciaPuesto', 1)
df = df.drop('PromedioOtrosEmpleos', 1)


# import preprocessing from sklearn
from sklearn import preprocessing
#print(df.columns)

le = preprocessing.LabelEncoder()

df_2 = df.apply(le.fit_transform)
#print(df_2.head())
#print(df_2.shape)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df_2)

df_2 = pd.DataFrame(data=imp.transform(df_2), columns = df_2.columns)

print(df_2.isnull().sum().sort_values(ascending=False).head())

X = df_2.drop('Calificacion', 1)
y = df_2.Calificacion

print(X.shape)

"""
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.40, random_state=1)

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

knn = KNeighborsClassifier(n_neighbors=6)
knn.fit(X_train, y_train)
print(knn)

y_pred_knn = knn.predict(X_test)
print(metrics.accuracy_score(y_test, y_pred_knn))
"""
"""
k_range = range(1, 26)
scores = []
for k in k_range:
	knn = knn = KNeighborsClassifier(n_neighbors=k)
	knn.fit(X_train, y_train)
	y_pred_knn = knn.predict(X_test)
	met = metrics.accuracy_score(y_test, y_pred_knn)
	#print("met {met} k {k}".format(met=met, k=k))
	scores.append(met)
"""

"""
plt.plot(k_range, scores)
plt.xlabel("Valor de K para knn")
plt.ylabel("Prueba de certeza")
plt.show()
"""

import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import metrics

#from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.40, random_state=100)

clf_gini = DecisionTreeClassifier()
clf_gini.fit(X_train, y_train)


y_pred = clf_gini.predict(X_test)
met = metrics.accuracy_score(y_test, y_pred)
print(met)


dotfile = open("../datasets/dtree2.1.dot", 'w')
tree.export_graphviz(clf_gini, out_file = dotfile, feature_names = X.columns)
dotfile.close()