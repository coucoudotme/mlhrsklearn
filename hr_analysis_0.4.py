import pandas as pd
import matplotlib.pyplot as plt

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


#df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)



print(df['Calificacion'].value_counts())
# import preprocessing from sklearn
from sklearn import preprocessing

labelEncoder = preprocessing.LabelEncoder()

df = df.apply(labelEncoder.fit_transform)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)

df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

#print(df.isnull().sum().sort_values(ascending=False).head())


for col_name in df.columns:
	unique_cat = len(df[col_name].unique())
	print("Feature '{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))

from sklearn.utils import resample

#UP SAMPLING
df_majority = df[df.Calificacion==0.0]
df_medium = df[df.Calificacion==2.0]
df_minority = df[df.Calificacion==1.0]

# Upsample minority class
df_minority_upsampled = resample(df_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_medium_upsampled = resample(df_medium, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_upsampled = pd.concat([df_majority, df_minority_upsampled])
df_upsampled = pd.concat([df_upsampled, df_medium_upsampled])

# Display new class counts
print(df_upsampled.Calificacion.value_counts())

X = df_upsampled.drop('Calificacion', 1)
y = df_upsampled.Calificacion


#	Cuenta la distribucion o frecuencia	
print(y.value_counts())

from sklearn.preprocessing import scale
Xs = scale(X)

from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(Xs, y, train_size=0.20, random_state=1)


from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

"""
k_range = range(1, 26)
scores = []
for k in k_range:
	knn = knn = KNeighborsClassifier(n_neighbors=k)
	knn.fit(X_train, y_train)
	y_pred_knn = knn.predict(X_test)
	met = metrics.accuracy_score(y_test, y_pred_knn)
	print("met {met} k {k}".format(met=met, k=k))
	scores.append(met)

plt.plot(k_range, scores)
plt.xlabel("Valor de K para knn")
plt.ylabel("Prueba de certeza")
plt.show()
"""

knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

print('k-NN score for test set: %f' % knn.score(X_test, y_test))
print('k-NN score for training set: %f' % knn.score(X_train, y_train))
#print(knn)

from sklearn.metrics import classification_report
y_true, y_pred = y_test, knn.predict(X_test)
print(classification_report(y_true, y_pred))

y_pred_knn = knn.predict(X_test)


print(metrics.accuracy_score(y_test, y_pred_knn))
#prediction = pd.DataFrame(y_pred_knn, columns=['predictions']).to_csv('../datasets/prediction.csv')

