"""
"" Balanced data
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


#df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

print(df['Calificacion'].value_counts())

from sklearn import preprocessing

labelEncoder = preprocessing.LabelEncoder()

df = df.apply(labelEncoder.fit_transform)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)

df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

X = df.drop('Calificacion', 1)
y = df.Calificacion

#	Cuenta la distribucion o frecuencia	
print(y.value_counts())

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
# Train model
clf_0 = LogisticRegression().fit(X, y)
 
# Predict on training set
pred_y_0 = clf_0.predict(X)
# How's the accuracy?
#print( accuracy_score(pred_y_0, y) )

# Should we be excited?
#print( np.unique( pred_y_0 ) ) #0.718796992481 [ 0.  2.] ignora completamenta la class 1 (NO CUMPLE ESPECTATIVAS)

from sklearn.utils import resample
#UP SAMPLING
df_majority = df[df.Calificacion==0.0]
df_medium = df[df.Calificacion==2.0]
df_minority = df[df.Calificacion==1.0]

# Upsample minority class
df_minority_upsampled = resample(df_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_medium_upsampled = resample(df_medium, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_upsampled = pd.concat([df_majority, df_minority_upsampled])
df_upsampled = pd.concat([df_upsampled, df_medium_upsampled])

# Display new class counts
print(df_upsampled.Calificacion.value_counts())

X = df_upsampled.drop('Calificacion', 1)
y = df_upsampled.Calificacion

#clf_0 = LogisticRegression().fit(X, y)
 
# Predict on training set
#pred_y_0 = clf_0.predict(X)
# How's the accuracy?
# print( accuracy_score(pred_y_0, y) ) #50%!!!!!!!!

#DOWN SALMPLING
df_majority = df[df.Calificacion==0.0]
df_medium = df[df.Calificacion==2.0]
df_minority = df[df.Calificacion==1.0]

# Upsample minority class
df_mayority_downsampled = resample(df_majority, 
                                 replace=False,     # sample with replacement
                                 n_samples=191,    # to match majority class
                                 random_state=123) # reproducible results

df_medium_downsampled = resample(df_medium, 
                                 replace=False,     # sample with replacement
                                 n_samples=191,    # to match majority class
                                 random_state=123) # reproducible results

df_downsampled = pd.concat([df_minority, df_mayority_downsampled])
df_downsampled = pd.concat([df_downsampled, df_medium_downsampled])

# Display new class counts
print(df_downsampled.Calificacion.value_counts())

X = df_downsampled.drop('Calificacion', 1)
y = df_downsampled.Calificacion

clf_2 = LogisticRegression().fit(X, y)
 
# Predict on training set
pred_y_0 = clf_2.predict(X)
# How's the accuracy?
print( accuracy_score(pred_y_0, y) ) #50%!!!!!!!!

#Area Under ROC CurvePython
from sklearn.metrics import roc_auc_score
# Predict class probabilities
prob_y_2 = clf_2.predict_proba(X)
 
# Keep only the positive class
prob_y_2 = [p[1] for p in prob_y_2]
print(prob_y_2[:5])
	
#print( roc_auc_score(y, prob_y_2) )

	
