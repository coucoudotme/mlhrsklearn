"""
""Penalize Algorithms (Cost-Sensitive Training)
"""
"""
"" Balanced data
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


#df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

print(df['Calificacion'].value_counts())

from sklearn import preprocessing

labelEncoder = preprocessing.LabelEncoder()

df = df.apply(labelEncoder.fit_transform)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)

df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

X = df.drop('Calificacion', 1)
y = df.Calificacion

from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

# Train model
from sklearn.ensemble import RandomForestClassifier
# Train model
clf_4 = RandomForestClassifier()
clf_4.fit(X, y)
 
# Predict on training set
pred_y_4 = clf_4.predict(X)
 
# Is our model still predicting just one class?
print( np.unique( pred_y_4 ) )
 
# How's our accuracy?
print( accuracy_score(y, pred_y_4) )
#81%