"""
"" Random Forest
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	# Hacer una prueba sacando lo de habilidades
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
#df = df.drop('CiudadNacimiento',1)
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

print(df['Calificacion'].value_counts())

from sklearn import preprocessing

labelEncoder = preprocessing.LabelEncoder()

df = df.apply(labelEncoder.fit_transform)

from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='median', axis=0)
imp.fit(df)

df = pd.DataFrame(data=imp.transform(df), columns = df.columns)

X = df.drop('Calificacion', 1)
y = df.Calificacion

from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

# Train model
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
from sklearn import metrics

from sklearn.preprocessing import scale
Xs = scale(X)

X_train, X_test, y_train, y_test = train_test_split(Xs, y, train_size=0.20, random_state=123)

clf_4 = RandomForestClassifier()
clf_4.fit(X_train, y_train)

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

y_true, y_pred = y_test, clf_4.predict(X_test)

print(classification_report(y_true, y_pred))
print(confusion_matrix(y_true, y_pred))

met = metrics.accuracy_score(y_test, y_pred)
print(met)

