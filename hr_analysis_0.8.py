"""
"" 
"""
from hr_dataset_loader import df, X, y
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.preprocessing import scale


#print(X.head())
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.20, random_state=1)

