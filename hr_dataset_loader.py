"""
"" Importacion base de dataset y clases necesarias
"" 2018.01.10
"""
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Imputer
from sklearn.utils import resample

"""
""	Carga de valores categóricos
""
""	Dataframe:
""	Feature 'FranjaEdad'
""	Feature 'Sexo' La eliminamos
""	Feature 'Nacionalidad' La eliminamos
""	Feature 'CiudadNacimiento' La eliminamos
""	Feature 'NivelMaximoEducacion'
""	Feature 'Idiomas'
""	Feature 'ExperienciaPuesto'
""	Feature 'PromedioOtrosEmpleos'
""	Feature 'HabilidadLiderazgo'
""	Feature 'HabilidadSocial'
""	Feature 'HabilidadCreatividad'
""	Feature 'HabilidadResponsabilidad'
""	Feature 'Calificacion'
"""
df = pd.read_csv("../datasets/HR_tabla_v1.0.csv",  warn_bad_lines=True)


df['CiudadNacimiento'] = ['CABA' if x == 'CAP FED' else 'OTHER' for x in df['CiudadNacimiento']]
df = df.drop('Nacionalidad',1)
df = df.drop('Sexo',1)

print(df['Calificacion'].value_counts())
print("\n")

print("Up sampling")
print("\n")

#UP SAMPLING
df_majority = df[df.Calificacion=='CUMPLE EXPECTATIVAS']
df_medium = df[df.Calificacion=='SUPERA EXPECTATIVAS']
df_minority = df[df.Calificacion=='NO CUMPLE EXPECTATIVAS']

# Upsample minority class
df_minority_upsampled = resample(df_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_medium_upsampled = resample(df_medium, 
                                 replace=True,     # sample with replacement
                                 n_samples=1404,    # to match majority class
                                 random_state=123) # reproducible results

df_upsampled = pd.concat([df_majority, df_minority_upsampled])
df_upsampled = pd.concat([df_upsampled, df_medium_upsampled])

# Display new class counts
print(df_upsampled.Calificacion.value_counts())

#X = df_upsampled.drop('Calificacion', 1)
#y = df_upsampled.Calificacion
df = df_upsampled

#mezcla la data
df = df.sample(frac=1).reset_index(drop=True)

"""
""	Cantidad de categoria por variables, esto permite revisar aquellas variables con muchas categorias distintas y reagruparlas
""	De esta manera se pueden agrupar aquellas que no tienen mucha frecuencia en otra llamada "Otros"
"""
df_features = df.drop('Calificacion', 1)
print("Características:")
print("----------------")

for col_name in df_features.columns:
	if df_features[col_name].dtypes =='object':
		unique_cat = len(df_features[col_name].unique())
		print("'{col_name}' tiene '{unique_cat}' categorias unicas".format(col_name=col_name, unique_cat=unique_cat))



"""
"" Permite generar variables dummies a traves de un conjunto de data
"""
def dummy_df(data, f_list):
	for x in f_list:
		dummies = pd.get_dummies(data[x], prefix=x, dummy_na=False)
		data = data.drop(x, 1)
		data = pd.concat([data, dummies], axis=1)

	return data

todo_list = ['FranjaEdad', 'NivelMaximoEducacion', 'Idiomas', 'ExperienciaPuesto', 'PromedioOtrosEmpleos']

X = dummy_df(df_features, todo_list)
y = df.Calificacion

label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)