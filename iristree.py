#iris example
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

iris = load_iris()
"""
""	Se muestran 4 columns ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']
""	Cada column es un feature y cada row representa una especie
"""
#print(iris.data)


"""
"" ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']
"""
#print(iris.feature_names) 

"""
""	Los targets son las especies de cada observación
""	00000000, 11111,222222  0=setosa, 1= versicolor, 2=virgnica
""	Los nombres de los targets son los nombres de cada especie.
""	['setosa' 'versicolor' 'virginica']
"""
#print(iris.target_names)


""" 
""	Features and responses deben estar en objetos distintos
""	Features and response should be numeric
""	Featues and responses should always be NumPy arrays
""	Shape: son las dos dimensiones de la data, (150L, 4L), cantidad de registros, columnas
"""
#print(iris.data.shape) 

"""
"" 	Feature matrix in X (capitalized -> matrix)
""	Response vector in y (lower -> vector)
"""	
X = iris.data
y = iris.target

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

# Models = Estimators
# Should be looking for the 1 nearest neighbor -> Tunning parameter
# n_neighbors=5 estará mirando 5 neighbors, con lo que variará nuestra prediccion probablemente
knn = KNeighborsClassifier(n_neighbors=1)
#print(knn)

"""
""	Fit the model with data (training)
""	El modelo aprende la relacion entre X (features matrix) e y(response vector)
""	El resultado se da dentro del clasificador
"""
knn.fit(X,y)

"""
""  Predict, retorna un nparray con valores.
""	Predice el response para una nueva observacion
""	Usa la informacion que aprendió durante el model training process
"" 	Espera una matriz de observaciones, en este caso le pasamos una
""	response = [2, 1] significa la prediccion para el primer juego de valores es 1, la segunda es 2
""	2 se corresponde con 'virginica', 1 se corresponde con 'versicolor' 
"""
one_obs = [[3,5,4,2]] 
two_obs = [[3,5,4,2], [5,4,3,2]]
response = knn.predict(two_obs)
print(response)


